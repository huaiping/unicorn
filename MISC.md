**Miscellaneous（Debian 9.6）**

Git 2.11
```
sudo apt-get install git

ssh-keygen -t rsa -b 4096 -C "xxx@xxx.cn"
cat ~/.ssh/id_rsa.pub
把所有字符粘贴到github的SSH Key输入框。

git config --global user.email "xxx@xxx.cn"
git config --global user.name "xxx"
git config --global push.default simple
git config --global credential.helper store
```
Composer 1.8.0 + Laravel 5.7
```
sudo apt install curl php-cli php-gd php-mbstring php-mysql php-xml
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
sudo composer create-project --prefer-dist laravel/laravel demo
php artisan serve
```
```
composer -V
composer self-update
composer config -g repo.packagist composer https://packagist.phpcomposer.com
```
```
sudo dd if=x.iso of=/dev/sdb bs=4M; sync

sudo umount /dev/sdb1
sudo mkfs.vfat -I /dev/sdb
```
Sudo
```
visudo
username ALL=(ALL) ALL
```
Fcitx 4.2.9.1
```
sudo dpkg-reconfigure locales
sudo apt install fcitx fcitx-sunpinyin fcitx-config-gtk2 fcitx-ui-classic
sudo apt install bash-completion fonts-wqy-microhei
sudo reboot
configure中添加sunpinyin
```
```
/etc/inputrc
set bell-style none

sudo rmmod pcspkr    #临时关闭beep
sudo echo "blacklist pcspkr" >> /etc/modprobe.d/blacklist    #永久关闭beep
modprobe pcspkr      #加载驱动
```
git撤销commit
```
git reset --hard <commit_id>
git push origin HEAD --force
```
```
https://mirrors.cloud.tencent.com
https://mirrors.163.com
https://mirrors.ustc.edu.cn
https://mirrors.tuna.tsinghua.edu.cn
https://mirror.lzu.edu.cn
https://mirror.bjtu.edu.cn
```
```
listen 443 ssl http2 default_server;
openssl dhparam -out /etc/nginx/ssl/dhparam.pem 2048
add the following line under ssl_certificate_key:
ssl_dhparam /etc/nginx/ssl/dhparam.pem;
```
```
ssh-keygen -t rsa
ls /root/.ssh/
ssh root@REMOTE_SERVER mkdir -p .ssh
cat /root/.ssh/id_rsa.pub | ssh root@REMOTE_SERVER 'cat >> /root/.ssh/authorized_keys'
ssh root@REMOTE_SERVER "chmod 700 .ssh; chmod 600 .ssh/authorized_keys"
nano /etc/ssh/sshd_config
RSAAuthentication yes
PubkeyAuthentication yes
AuthorizedKeysFile      %h/.ssh/authorized_keys
/etc/init.d/sshd restart
```
```
PHP and MSSQL on Ubuntu
sudo apt install php5-sybase
```
Certbot
```
sudo certbot renew --dry-run
crontab -e
30 2 * * 1 /usr/bin/certbot renew  >> /var/log/le-renew.log
```
```
帐号权限
useradd test     #centos会在home下新增同名目录 ubuntu需添加-m
useradd test -m

passwd test
gpasswd -a test sudo
su test
```
backup.sh
sh backup.sh    #执行
```
#!/bin/sh

backUpFolder=/home/xxx/backup
date_now=`date +%Y_%m_%d_%H%M`
backFileName=mall_$date_now

cd $backUpFolder
mkdir -p $backFileName

mongodump -h 127.0.0.1:27017 -d mall -u mall -p 123 -o $backFileName

tar zcvf $backFileName.tar.gz $backFileName
rm -rf $backFileName
```
```
sudo apt install apache2 libapache2-mod-passenger mysql-server

sudo gem update
sudo gem install bundler

sudo vi /etc/apache2/mods-available/passenger.conf
在<IfModule mod_passenger.c>和</IfModule>之间添加PassengerDefaultUser www-data

$ sudo vi /etc/apache2/sites-available/000-default.conf
在<VirtualHost *:80>和</VirtualHost>之间加入如下Directory元素
<Directory /var/www/html/redmine>
    RailsBaseURI /redmine
    PassengerResolveSymlinksInDocumentRoot on
</Directory>
如果在根域名下访问，将000-default.conf的DocumentRoot由/var/www/html改为/var/www/html/redmine
```
```
sudo npm cache clean -f
sudo npm install -g n
sudo n stable 或 sudo n 10.14.2
```

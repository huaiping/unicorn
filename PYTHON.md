**Python笔记（Debian 9.6 + Python 3.5.3 + Django 2.1.5）**

python3 -V
```
sudo apt install apache2 libapache2-mod-wsgi-py3 python3-pip mysql-server libmariadbd-dev
sudo pip3 install Django
sudo pip3 install mysqlclient
```
/etc/pip.conf
```
[global]
trusted-host = mirrors.aliyun.com
index-url = https://mirrors.aliyun.com/pypi/simple
extra-index-url = https://pypi.tuna.tsinghua.edu.cn/simple
```
/etc/apache2/sites-available/000-default.conf
```
<VirtualHost *:80>
    ServerName xxx.net
    WSGIScriptAlias / /var/www/pandora/django.wsgi
    <Directory "/var/www/pandora">
        Options FollowSymLinks Indexes
        AllowOverride all
        Require all granted
    </Directory>

    Alias /robots.txt /var/www/pandora/static/robots.txt
    Alias /static /var/www/pandora/static
    <Location "/static">
        SetHandler None
    </Location>
    <Directory "/var/www/pandora/static">
        Require all granted
    </Directory>

    ErrorLog "/var/log/apache2/py-error.log"
    CustomLog "/var/log/apache2/py-access.log" combined
</VirtualHost>
```
```
django-admin.py startproject pandora
cd pandora
```
pandora/django.wsgi
```
# -*- coding: utf-8 -*-

import os
import sys
import django.core.handlers.wsgi

os.environ['DJANGO_SETTINGS_MODULE'] = 'pandora.settings'
app_path = "/var/www/pandora/"
sys.path.append(app_path)

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
```
pandora/setting.py
```
LANGUAGE_CODE = 'zh_CN'
TIME_ZONE = 'Asia/Shanghai'

INSTALLED_APPS = ()注册模块

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'qdm160638220_db',
        'USER': 'qdm160638220',
        'PASSWORD': '',
        'HOST': 'qdm160638220.my3w.com',
        'PORT': '3306',
        'OPTIONS': {
            'autocommit': True,
        }
    }
}

STATIC_ROOT = os.path.join(BASE_DIR,'static')
STATICFILES_DIRS = (
    '/usr/local/lib/python3.5/dist-packages/django/contrib/admin/static/',
)
```
```
python3 manage.py startapp guestbook
python3 manage.py check
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py syncdb
python3 manage.py collectstatic
python3 manage.py createsuperuser
```
```
pip3 --version
pip3 install --upgrade pip
pip3 search Django
pip3 list --outdated
```

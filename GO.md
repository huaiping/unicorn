**Go笔记（Debian 9.6 + Go 1.7）**
```
sudo apt install golang
```
```
go version
mkdir ~/workspace
echo 'export GOPATH="$HOME/workspace"' >> ~/.bashrc
source ~/.bashrc
```
```
mkdir -p ~/workspace/src/hello
nano ~/workspace/src/hello/hello.go
cd ~/workspace/src/hello
go build
./hello
```
```
go get github.com/go-sql-driver/mysql
```
```
package main

import _ "github.com/go-sql-driver/mysql"
import "database/sql"
import "fmt"

func main() {
    db, err := sql.Open("mysql", "root:123456@tcp(localhost:3306)/test?charset=utf8")
    if err != nil {
        panic(err.Error())
    }
    defer db.Close()

    err = db.Ping()
    if err != nil {
        panic(err.Error())
    }

    rows, err := db.Query("select id, companyname from baseinfo")
    defer rows.Close()
    for rows.Next() {
        var id int
        var name string
        err = rows.Scan(&id, &name)
        fmt.Printf("rows id = %d, value = %s", id, name)
    }
    err = rows.Err()
    if err != nil {
        panic(err.Error())
    }
}
```

### Unicorn [![license](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat)](https://gitlab.com/huaiping/unicorn/blob/master/LICENSE)
```
git clone git@github.com:huaiping/unicorn.git  
cd unicorn

git submodule add git@github.com:bcit-ci/CodeIgniter.git CodeIgniter  
git submodule add git@github.com:kindsoft/kindeditor.git KindEditor  
git submodule add git@github.com:cdnjs/cdnjs.git cdnjs
```

添加时使用，以*CodeIgniter*为例
```
git add CodeIgniter  
git commit -m "added"  
git push
```

更新时使用，以*cdnjs*为例
```
cd cdnjs  
git pull origin master  
cd ..  
git add cdnjs  
git commit -m "updated"  
git push
```

删除时使用，以*KindEditor*为例
```
vim .gitmodules (Delete the relevant section from .gitmodules)  
git add .gitmodules  
vim .git/config (Delete the relevant section from .git/config)  
git rm --cached KindEditor  
rm -rf .git/modules/KindEditor  
git commit -m "removed"  
rm -rf KindEditor
```

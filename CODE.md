**代码学习笔记**

C
```c
#include <stdio.h>
int main()
{
    printf("Content-Type: text/html\n\n");
    printf("Hello, world\n");
    return 0;
}
#sudo gcc hello.c -o hello.cgi
```
Golang
```go
package main
import "fmt"
func main() {
    fmt.Printf("Hello, world\n")
}
$go run hello.go
```
JSP
```jsp
<%@ page contentType="text/html;charset=utf-8"%>
<%@ page import="java.sql.*"%>
<html>
<body>
<%
Connection con;
Statement sql;
ResultSet rs;
try{Class.forName("com.mysql.jdbc.Driver").newInstance();}
catch(Exception e){out.print(e);}
try{
    String uri="jdbc:mysql://localhost:3306/mysql";//最后的mysql是数据库名
    con=DriverManager.getConnection(uri,"root","dbpass");
    sql=con.createStatement();
    rs=sql.executeQuery("SELECT * FROM user");
    out.print("<table border=2>");
    out.print("<tr>");
    out.print("<th width=100>"+"Host");
    out.print("</tr>");
    while(rs.next()){
        out.print("<tr>");
        out.print("<td>"+rs.getString(1)+"</td>");
        out.print("</tr>");
    }
    out.print("</table>");
    con.close();
}
catch(SQLException e1){out.print(e1);}
%>
</body>
</html>
```
Python
```python
#!/usr/bin/python
import cgi
import MySQLdb
db = MySQLdb.connect("localhost","root","dbpass","phpmyadmin")
cursor = db.cursor()
cursor.execute("SELECT VERSION()")
data = cursor.fetchone()
print 'Content-Type:text/html\r\n\r\n'
print 'Database version : %s ' % data
db.close()
```
Python
```python
# -*- coding: utf-8 -*-
#mysqldb
import time, MySQLdb
#连接
conn=MySQLdb.connect(host="localhost",user="root",passwd="",db="green",charset="utf8")
cursor = conn.cursor()
#写入
sql = "insert into user(email,password) values(%s,%s)"
param = ("zts1993","0000")
n = cursor.execute(sql,param)
print n
#更新
sql = "update user set name=%s where id=1"
param = ("zts1993")
n = cursor.execute(sql,param)
print n
#查询
n = cursor.execute("select * from user")
for row in cursor.fetchall():
    for r in row:
        print r
#删除
sql = "delete from user where name=%s"
param =("zts1993")
n = cursor.execute(sql,param)
print n
cursor.close()
#关闭
conn.close()
```
Ruby
```ruby
# test.rb - test MySQL script using Ruby DBI module
require "dbi"
begin
    # connect to the MySQL server
    dbh = DBI.connect("dbi:Mysql:mysql:localhost", "root", "dbpass")
    # get server version string and display it
    row = dbh.select_one("SELECT VERSION()")
    puts "Server version: " + row[0]
rescue DBI::DatabaseError => e
    puts "An error occurred"
    puts "Error code: #{e.err}"
    puts "Error message: #{e.errstr}"
ensure
    # disconnect from server
    dbh.disconnect if dbh
end
```
